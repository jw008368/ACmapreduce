package acassignment;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class Flights 
{
	//Variables created from the data given, each made private to limit the scope.
	private String flightID, deptFAA, destFAA, dept_time_gmt, flight_time;
	private List<String> passengers;

	//The flight constructor - takes all defined variables as parameters.
	Flights(String id, String deptFAA, String destFAA, String timeGMT, String flightTime)
	{
		//Each variable is assigned.
		this.flightID = id;
		this.deptFAA = deptFAA;
		this.destFAA = destFAA;
		this.dept_time_gmt = timeGMT;
		this.flight_time = flightTime;
		
		//Initialise the list of passengers into an ArrayList.
		passengers = new ArrayList<String>();
	}
	
	// Add passenger IDs in the ArrayList
	public void addPassengers(String pass_id)
	{
		passengers.add(pass_id);
	}
	
	//Getter method to retrieve the passenger list.
	public List<String> getPassengers()
	{
		return passengers;
	}
	
	//Get the length of passengers list and return that number.
	public int returnNumPass()
	{
		int num = 0;
		
		for(int i = 0; i < passengers.size(); i++)
		{
			num++;
		}
		
		return num;
	}
	
	//Output the passengers
	public void printPassengers()
	{
		for(int i = 0; i < passengers.size(); i++)
		{
			System.out.println("\t\t" + passengers.get(i));
		}
	}
	
	//Print the passengers to PrintStream p1 buffer.
	public void printStreamOutput(PrintStream p1)
	{
		//String output = "";
		for(int i = 0; i < passengers.size(); i++)
		{
			p1.println("\t\t" + passengers.get(i));
		}
	}
	
	//Add list of strings(s1) to passengers list
	public void mergePassengers(List<String> psg)
	{
		passengers.addAll(psg);
	}
	
	
	//All getter methods below
	
	//Get flightID
	public String getFlightID()
	{
		return flightID;
	}
	
	//Get deptFAA
	public String getDeptFAA()
	{
		return deptFAA;
	}
	
	//Get destFAA
	public String getDest()
	{
		return destFAA;
	}
	
	//Get departure time
	public String getDeptTime()
	{
		return dept_time_gmt;
	}
	
	//Get flight time
	public String getFlightTime()
	{
		return flight_time;
	}
}


