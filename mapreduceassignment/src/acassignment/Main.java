package acassignment;

//Import file that manages the jobs
import acassignment.ManageJobs;

public class Main 
{
	public static void main(String[] args)
	{	
		//Create first job variable.
		ManageJobs firstJob;
		try {
			firstJob = new ManageJobs();	//Initiate the variable that was created.
			firstJob.start();				//Start method used to run job.
		} catch (Exception e) {
			e.printStackTrace();			//Auto-generated error exception handler. 
		}
	}
}
