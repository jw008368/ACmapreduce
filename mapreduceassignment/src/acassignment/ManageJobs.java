package acassignment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ManageJobs extends Thread
{
	PrintStream ffaErrors, passengerErrors;
	
	private String passengerCSV, airport30CSV;
	private List<String[]> passengerDatafile, airport30Datafile;
	private int DataSize, batch;
	
	//Mappers
	private Mapper node1, node2, node3, node4, node5;
	
	//Outputs
	private Map<String, Flights> firstOutput, secondOutput, thirdOutput, fourthOutput, fifthOutput;
	
	private ArrayList<Map<String, Flights>> shuffleSortInputs; 
	private ArrayList<Sort> shuffleSortThreads;
	private List<String> finalData;
	private Map<String, Flights> output;
	
	private static final long ONE_MINUTE_IN_MILLIS = 60000;	//Milliseconds output

	//Manage Job Controller
	 
	ManageJobs() throws Exception
	{
		//File Outputs
		ffaErrors = new PrintStream("Errors - FAA Code.txt");
		passengerErrors = new PrintStream("Errors - Passengers.txt");
		
		//Data Files
		passengerCSV = "doc/AComp_Passenger_data.csv";
		airport30CSV = "doc/Top30_airports_LatLong.csv";
		passengerDatafile = new ArrayList<String[]>();
		airport30Datafile = new ArrayList<String[]>();
		
		//Initialising
		node1 = new Mapper();
		node2 = new Mapper();
		node3 = new Mapper();
		node4 = new Mapper();
		node5 = new Mapper();
		
		firstOutput = new LinkedHashMap<String, Flights>();
		secondOutput = new LinkedHashMap<String, Flights>();
		thirdOutput = new LinkedHashMap<String, Flights>();
		fourthOutput = new LinkedHashMap<String, Flights>();
		fifthOutput = new LinkedHashMap<String, Flights>();
		
		shuffleSortInputs = new ArrayList<Map<String, Flights>>();
		output = new LinkedHashMap<String, Flights>();
		finalData = new ArrayList<String>();
		shuffleSortThreads = new ArrayList<Sort>();
		
		//Open CSV data files
		openCSV1(); 
		openCSV2();
		checkCSV2Syntax();
		dataVerifification();
		
		//Deletes multiple/duplicate data
		deleteDuplicateData();
		
		DataSize = passengerDatafile.size();
		batch = DataSize / 5; //Divided by nodes
	}

	public void run()
	{
		for(int i = 0; i < batch; i++)
		{
			node1.addPassengerData(passengerDatafile.get(i));
		}
		
		for(int i = batch; i < batch*2; i++)
		{
			node2.addPassengerData(passengerDatafile.get(i));
		}
		
		for(int i = batch * 2; i < batch*3; i++)
		{
			node3.addPassengerData(passengerDatafile.get(i));
		}
		
		for(int i = batch * 3; i < batch*4; i++)
		{
			node4.addPassengerData(passengerDatafile.get(i));
		}
		
		for(int i = batch * 4; i < batch*5; i++)
		{
			node5.addPassengerData(passengerDatafile.get(i));
		}
		
		if(batch*5 < passengerDatafile.size())
		{
			
			for(int i = batch*5; i < passengerDatafile.size(); i++)
			{
				node5.addPassengerData(passengerDatafile.get(i));
			}
		}
		
		node1.start();
		node2.start();
		node3.start();
		node4.start();
		node5.start();
		
		try {
			node1.join();
			node2.join();
			node3.join();
			node4.join();
			node5.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		firstOutput = node1.returnMap();
		secondOutput = node2.returnMap();
		thirdOutput = node3.returnMap();
		fourthOutput = node4.returnMap();
		fifthOutput = node5.returnMap();
		
		shuffleSortInput(); 
		
		setUnique();
		reduce();
		
		for (Map.Entry<String, Flights> entry : output.entrySet())
		{
			System.out.println(entry.getKey());
		}
		printData();
	}
	
	//First CSV
	private void openCSV1()
	{
		String line = ""; 
		String tempLine = "";
        String splitter = ",";
        String airport, airport_FAA, lat, longt;
        int count = 1;

        try (BufferedReader br = new BufferedReader(new FileReader(airport30CSV))) 
        {        	
            while ((line = br.readLine()) != null)
            {
            	String[] lineData = new String[4];
            	lineData = line.split(splitter);
            	
            	if(lineData.length == 4)
            	{
                	airport = lineData[0]; //PASENGER ID and ID checks            	
                	airport_FAA = lineData[1];
                	lat = lineData[2];
                	longt = lineData[3];
                	
                	if(checkCSV1Syntax(airport, airport_FAA, lat, longt))
                	{
                		airport30Datafile.add(lineData);
                	}
                	else
                	{
                		System.out.println("Deleted corrupt airport no." + count);
                		count++;
                		continue;
                	}
                	
            	}
            }

        } catch (IOException  e) {
        }
	}
		
	//Open 2nd CSV
	private void openCSV2()
	{
        String line = "";
        String splitter = ","; 
        String passenger_id, flight_id, frm_airport_FAA, dest_airport_FAA, dept_time_gmt, flight_time_mins;
        int count = 1;
        boolean firstError = false;
	

        try (BufferedReader br = new BufferedReader(new FileReader(passengerCSV))) 
        {

            while ((line = br.readLine()) != null) 
            {
            	String[] lineData = line.split(splitter); 
 
            	if(firstError == false)
            	{
            		passenger_id = lineData[0].substring(3);
            		lineData[0] = passenger_id;
            		firstError = true;
            	}
            	
            	else
            	{
            		passenger_id = lineData[0];
            	}
            	 //PASENGER ID and ID checks            	
            	flight_id = lineData[1];
            	frm_airport_FAA = lineData[2];
            	dest_airport_FAA = lineData[3];
            	dept_time_gmt = lineData[4];
            	flight_time_mins = lineData[5];
            	
            	if(!dept_time_gmt.equals("0"))
            	{
                    passengerDatafile.add(lineData);
                    continue;

            	}
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	//Check for errors and exclude
	 
	private void checkCSV2Syntax()
	{	
		for(int i = 0; i < passengerDatafile.size(); i++)
		{
		
			String s1 = passengerDatafile.get(i)[0];
			String s2 = passengerDatafile.get(i)[1];
			String s3 = passengerDatafile.get(i)[2];
			String s4 = passengerDatafile.get(i)[3];
			String s5 = passengerDatafile.get(i)[4];
			String s6 = passengerDatafile.get(i)[5];
			
			Pattern p1 = Pattern.compile("[A-Z]{3}\\d{4}[A-Z]{2}\\d"); //XXXnnnnXXn - Passenger ID
			Pattern p2 = Pattern.compile("[A-Z]{3}\\d{4}[A-Z]"); //XXXnnnnX - Flight ID
			Pattern p3 = Pattern.compile("[A-Z]{3}"); //xxx - FAA code
			Pattern p4 = Pattern.compile("\\d{10}"); //n[10] - dept time (gmt) in EPOCH
			Pattern p5 = Pattern.compile("\\d{1,4}"); //n[1-4] - flight time (mins)
			
		   	Matcher m1 = p1.matcher(s1); //passenger id
		   	Matcher m2 = p2.matcher(s2); //flight id
		   	Matcher m3 = p3.matcher(s3); //to FAA
		   	Matcher m4 = p3.matcher(s4); //from FAA
		   	Matcher m5 = p4.matcher(s5); //dept time
		   	Matcher m6 = p5.matcher(s6); //flight time
		   	
		   	if(s1.equals(null))
		   	{
		   		passengerDatafile.remove(i);
		   	}
		   	
		   	
		   	if(!m1.matches())
		   	{
		   		passengerDatafile.remove(i);
		   	}
		   	
		   	else if(!m2.matches())
		   	{
		   		if(FlightIDCorrection(i))
		   		{
		   			//Nothing
		   		}
		   		
		   		else
		   		{
		   			passengerDatafile.remove(i);
		   		}
		   		
		   	}
	   	
		}
  
	}
	
	//Check the first file for errors and correct
	private boolean checkCSV1Syntax(String s1, String s2, String s3, String s4)
	{
		Pattern p1 = Pattern.compile("[A-Z\\s/]{3,20}"); //Airport Name
		Pattern p2 = Pattern.compile("[A-Z]{3}"); //XXX
		Pattern p3 = Pattern.compile("\\p{Punct}?\\d{1,2}\\p{Punct}\\d{6}"); //xxx - FAA code
		Pattern p4 = Pattern.compile("\\p{Punct}?\\d{1,3}\\p{Punct}\\d{6}"); //xxx - FAA code

	   	Matcher m1 = p1.matcher(s1); //passenger id
	   	Matcher m2 = p2.matcher(s2); //flight id
	   	Matcher m3 = p3.matcher(s3); //to FAA
		Matcher m4 = p4.matcher(s4); //to FAA
	   	
	   	if(!m1.matches())
	   	{
	   		return false;
	   	}
	   	
	   	else if(!m2.matches())
	   	{
	   		return false;
	   	}
	   	else if(!m3.matches())
	   	{
	   		return false;
	   	}
	   	else if(!m4.matches())
	   	{
	   		return false;
	   	}
	   	
	   	else
	   	{
	   		return true;
	   	}
	}
		
	//Verify data within both files for matches and check for differences 
	private void dataVerifification()
	{
		int count = 0;
		for(int i = 0; i < passengerDatafile.size(); i++)
		{
			/*
			 * if either of these return false, that means in the passenger data, one of the FAA codes is incorrect,
			 * we need to find the correct value. For this, all we need to do is match flight id's
			 */
			if(passengerDataVerification(passengerDatafile.get(i)[2]) == false)
			{
				//data fix
				matchFAA(i, 2, passengerDatafile.get(i)[1]);
			}
			else if(passengerDataVerification(passengerDatafile.get(i)[3]) == false)
			{
				//data fix - check this passengers flight ID and match the aiport FAA code
				matchFAA(i, 3, passengerDatafile.get(i)[1]);
			}
		}
	}
	
	/*
	 * Match passenger to FAA code
	 */
	private boolean passengerDataVerification(String faa)
	{		
	
		for(int i = 0; i < 26; i++)
		{
			String tempFAA = airport30Datafile.get(i)[1];
			if(faa.equals(tempFAA))
			{
				return true;
			}
		}


		return false;
	}

	/*
	 * Match FAA code for data correction
	 */
	private void matchFAA(int index, int array, String flightID)
	{
		

			for(int i = 0; i < passengerDatafile.size(); i++)
			{
				if(i == index)
				{
					continue;
				}
				
				//If flightID matches
				if(passengerDatafile.get(i)[1].equals(flightID))
				{
					ffaErrors.println("Error Code: " + passengerDatafile.get(index)[array] + " - Correct Code: " + passengerDatafile.get(i)[array]);
					System.out.println("Error Code: " + passengerDatafile.get(index)[array] + " - Correct Code: " + passengerDatafile.get(i)[array]);					
					passengerDatafile.get(index)[array] = passengerDatafile.get(i)[array];
					break;
				}
			}
		
		} 
		
	
	/*
	 * Correct the FAA code errors within input data
	 */
	private boolean FlightIDCorrection(int index)
	{
		

		String tempFromFAA = passengerDatafile.get(index)[3];
		String tempDestFAA = passengerDatafile.get(index)[3];
		String tempDeptTime = passengerDatafile.get(index)[4];
		
		for(int i = 0; i < passengerDatafile.size(); i++)
		{
			if(tempDeptTime.equals(passengerDatafile.get(i)[4]))
			{
				if(tempFromFAA.equals(passengerDatafile.get(i)[2]))
				{
					if(tempDestFAA.equals(passengerDatafile.get(i)[3]))
					{
						System.out.println("Error ID: " + passengerDatafile.get(index)[1] + " - Fixed ID: " + 
						passengerDatafile.get(i)[1]);
						passengerDatafile.get(index)[1] = passengerDatafile.get(i)[1];
						return true;
					}
				}
			}
		}
	
		
		return false;
	}
	
	/*
	 * Delete duplicate data
	 */
	private void deleteDuplicateData()
	{
		for(int i = 0; i < passengerDatafile.size(); i++)
		{
			String passiD = passengerDatafile.get(i)[0];
			
			for(int j = 0; j < passengerDatafile.size(); j++)
			{
				if(i != j)
				{
					if(passiD.equals(passengerDatafile.get(j)[0]))
					{
						passengerErrors.println("Removed Data: " + passengerDatafile.get(j)[0]);
						System.out.println("Removed Data: " + passengerDatafile.get(j)[0]);
						passengerDatafile.remove(j);
						j--;
						
					}
				}
			}
		}
	}
	
	/*
	 * Prepare intermediate outputs for shufflesort input
	 */
	private void shuffleSortInput()
	{
		
		for(Map.Entry<String, Flights> entry: firstOutput.entrySet())
		{
			Map<String, Flights> tempMap = new LinkedHashMap<String, Flights>();
			tempMap.put(entry.getKey(), entry.getValue());
			shuffleSortInputs.add(tempMap);		
		}
		
		for(Map.Entry<String, Flights> entry: secondOutput.entrySet())
		{
			Map<String, Flights> tempMap = new LinkedHashMap<String, Flights>();
			tempMap.put(entry.getKey(), entry.getValue());
			shuffleSortInputs.add(tempMap);		
		}
		
		for(Map.Entry<String, Flights> entry: thirdOutput.entrySet())
		{
			Map<String, Flights> tempMap = new LinkedHashMap<String, Flights>();
			tempMap.put(entry.getKey(), entry.getValue());
			shuffleSortInputs.add(tempMap);		
		}
		
		for(Map.Entry<String, Flights> entry: fourthOutput.entrySet())
		{
			Map<String, Flights> tempMap = new LinkedHashMap<String, Flights>();
			tempMap.put(entry.getKey(), entry.getValue());
			shuffleSortInputs.add(tempMap);		
		}
		
		for(Map.Entry<String, Flights> entry: fifthOutput.entrySet())
		{
			Map<String, Flights> tempMap = new LinkedHashMap<String, Flights>();
			tempMap.put(entry.getKey(), entry.getValue());
			shuffleSortInputs.add(tempMap);		
		}
	}
	
	/*
	 * Find unique data sets for intermediate input (reducer)
	 */
	private void setUnique()
	{
		finalData.add(shuffleSortInputs.get(0).keySet().iterator().next().toString());
		
		for(int i = 1; i < shuffleSortInputs.size(); i++)
		{
			for(Map.Entry<String, Flights> entry: shuffleSortInputs.get(i).entrySet())
			{
				if(!NotUnique(entry.getKey())) //if returned false, data is not in uniqueData set
				{
					finalData.add(entry.getKey());
				}
			}
		}	
	}
	
	//Unique check
	private boolean NotUnique(String s1)
	{
		for(int i = 0; i < finalData.size(); i++)
		{
			if(s1.equals(finalData.get(i)))
			{
				return true;
			}
		}
		
		return false;
	}
	
	//Reducer thread
	private void reduce()
	{
		for(int i = 0; i < finalData.size(); i++)
		{
			shuffleSortThreads.add(new Sort(finalData.get(i))); //create shufflesort thread for this flight
			
			for(int j = 0; j < shuffleSortInputs.size(); j++)
			{
				for (Map.Entry<String, Flights> entry : shuffleSortInputs.get(j).entrySet())
				{
					if(finalData.get(i).equals(entry.getKey())) //condition to check which data gets sent to which SS thread
					{
						//if true, send to thread created in i
						shuffleSortThreads.get(i).sortData(entry.getKey(), entry.getValue());
					}
					
					else
					{
						continue;
					}
				}
			}
			
			shuffleSortThreads.get(i).start();
		}
		
		for(int i = 0; i < shuffleSortThreads.size(); i++)
		{
			try {
				shuffleSortThreads.get(i).join();
				output.putAll(shuffleSortThreads.get(i).returnReduce());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	//Create the output files
	private void printData()
	{
		try {
		//----------------------------OBJECTIVE 1-------------------------------
		//Number of flights from each airport and unused airports
	
			PrintStream file1 = new PrintStream("Airports.txt"); //Output file
			String output1 = "";
			for(int i = 0; i < airport30Datafile.size() ; i++)
	        {
				int num = 0;
				String airportName = airport30Datafile.get(i)[0];
				String airportFAA = airport30Datafile.get(i)[1];	
				
				for (Map.Entry<String, Flights> entry : output.entrySet())
	        	{
					if(entry.getValue().getDeptFAA().equals(airportFAA))
					{
						num++;
					}
	        	}
				file1.println("Airport: " + airportName + " (" + airportFAA + ")");
				file1.println("Number of Flights: " + num);	
				file1.println("------------------");
	        }
        
		
		//--------------------------Objectives 2/3---------------------------
		String output2 = "";

		System.out.println(output1);
        System.out.println(output2);
        
        
			PrintStream file2 = new PrintStream("Flights and Passengers.txt"); //Print Stream
			
			for(int i = 0; i < airport30Datafile.size() ; i++)
	        {
	        	String airportName = airport30Datafile.get(i)[0];
	        	String airportFAA = airport30Datafile.get(i)[1];
	        	file2.println("========================================================================"); //Line between airports
	        	file2.println("Airport: " + airportName + " (" + airportFAA + ")");
	        	
	        	for (Map.Entry<String, Flights> entry : output.entrySet())
	        	{
	        		if(entry.getValue().getDeptFAA().equals(airportFAA))
	        		{
	        			//Calculate dept time/date
	        			String date = entry.getValue().getDeptTime();
	        			long epoch = Long.parseLong(date);
	        			epoch = epoch * 1000;
	        			Date d = new Date(epoch);
	        			
	        			//Calculate arrival time
	        			String length = entry.getValue().getFlightTime();
	        			long arrivalMin = Long.parseLong(length);
	        			epoch = epoch + (arrivalMin * 60000);
	        			Date arrival = new Date(epoch);

	        			file2.println("\tFlight ID: " + entry.getValue().getFlightID());
	        			file2.println("\tDept. Time: " + d.toString());
	        			file2.println("\tArrival Time: " + arrival.toString());
	        			file2.println("\tFlight Length (mins)" + length);
	        			file2.println("\tNo. of Passengers: " + entry.getValue().returnNumPass());
	        			file2.println("\tPassengers:");
	        			entry.getValue().printStreamOutput(file2);
	        			;	
	        		}
	        	}
	        }
		} 
        catch (FileNotFoundException e) 
        {
			e.printStackTrace();
		}
	}
}
	

