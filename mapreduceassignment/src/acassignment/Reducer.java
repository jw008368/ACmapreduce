package acassignment;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class Reducer extends Thread
{

	private ArrayList<Pair<String, Flights>> reducerInp;
	private String flightID;
	private Flights flight;
	
	//Create reduce thread with input as ArrayList<Pair<String, Flights>>
	Reducer(ArrayList<Pair<String, Flights>> input)
	{
		reducerInp = new ArrayList<Pair<String, Flights>>();
		reducerInp = input;
	}
	
	public void run()
	{
		flightID = reducerInp.get(0).getFirst();
		flight = reducerInp.get(0).getSecond();
		for(int i = 1; i < reducerInp.size(); i++)
		{
			flight.mergePassengers(reducerInp.get(i).getSecond().getPassengers());
		}
	}
	
	//Return Data as a LinkedHashMap
	public LinkedHashMap<String, Flights> returnReducedData()
	{
		LinkedHashMap<String, Flights> temp = new LinkedHashMap<String, Flights>();
		temp.put(flightID, flight);
		return temp;
	}
	
}

