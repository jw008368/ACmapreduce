package acassignment;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class Sort extends Thread
{
	
	private ArrayList<Pair<String, Flights>> d;		//The data
	private LinkedHashMap<String, Flights> reducerOutput;	//Output
	private String tname; 					//Name of thread
	private Reducer reducerThread;			//Reduce Thread
	
	Sort(String name)
	{
		tname = name;
		d = new ArrayList<Pair<String, Flights>>();
	}
	
	public void run()
	{
		reducerThread = new Reducer(d);
		reducerThread.start();
		try {
			reducerThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void sortData(String s1, Flights f1)
	{
		Pair<String, Flights> tmpPair = new Pair<String, Flights>();
		tmpPair.setFirst(s1);
		tmpPair.setSecond(f1);
		d.add(tmpPair);
		
	}
	
	public LinkedHashMap<String, Flights> returnReduce()
	{
		reducerOutput = new LinkedHashMap<String, Flights>();
		reducerOutput = reducerThread.returnReducedData();
		
		return reducerOutput;
	}
}
